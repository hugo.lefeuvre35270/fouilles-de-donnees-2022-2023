###K-means###########
#Est ce que la mortalité des patients s'explique par la taille et le nbr de mutation ?

install.packages('ggplot2')
install.packages("tidyverse")
install.packages("stringr") 
install.packages("dplyr")               

library(tidyverse)
library(igraph)
library(stringr)

table = read_csv("fouille_breast.csv")
View(table)
colnames(table)

#### selectionner des colonnes précise ######
### clustering s'applique sur des données quantitative ####
table_filtrée <- table %>% select (tumor_size,mutation_count,death_from_cancer)
View(table_filtrée)
## les differents cause de mort 
table_filtrée.labels=table_filtrée$death_from_cancer
table(table_filtrée.labels)
###### garder que ceux qui sont mort à cause du cancer et ceux qui sont encore vivant

table_filtré = table_filtrée %>% filter(death_from_cancer == "Died of Disease" | death_from_cancer =="Living")
#### transformer les cases vides en NA
table_filtré[table_filtré==""]<-NA

### éliminer les lignes ou il y a des valeurs manquantes
data= table_filtré %>% drop_na()
View(data)


# extraire nos 2 variables quantitatives
mydata = select(data , c(1,2))
view(mydata)


## normaliser nos valeurs 

mydata_normalie = scale(mydata)
view(mydata_normalie)


## déterminer le nbr de cluster par la méthode de elbow 
install.packages("factoextra")
library(factoextra)

fviz_nbclust(mydata_normalie , kmeans ,method="wss")+  #wss=within sum of squares
  labs(subtitles="Elbow method")

KM =kmeans(mydata_normalie , centers = 4 , iter.max=100 , nstart=100)


fviz_cluster(KM , data=mydata_normalie)

table(data$death_from_cancer , KM$cluster )


