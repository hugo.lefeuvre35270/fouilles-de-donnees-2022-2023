install.packages('ggplot2')
install.packages("tidyverse")
install.packages("stringr") 
install.packages("dplyr")                

library(tidyverse)
library(igraph)
library(stringr)
library(dplyr)
table = read_csv("fouille_breast.csv")
View(table)
colnames(table)
name <- colnames(table)

#### selectionner des colonnes précise ######

table_filtré <- table %>% select (type_of_breast_surgery, cancer_type_detailed , cellularity , er_status_measured_by_ihc , inferred_menopausal_state , primary_tumor_laterality)
View(table_filtré)

table_filtré[table_filtré==""]<-NA

### éliminer les lignes ou il y a des valeurs manquantes
data= table_filtré %>% drop_na()
View(data)

rules =as.data.frame(data)
View(rules)
write.csv(rules,"rule_data_association.csv", row.names = FALSE )





