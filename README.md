# Fouilles de données 2022-2023

Ce projet a pour objectif de tester 3 méthodes de fouilles de données différentes sur un jeu de données contenant des informations sur 1904 individus atteints du cancer du sein.

## Data

Notre jeu de données est constitué de 1904 individus et de 504 colonnes correspondant à des caractéristiques sur le cancer du sein, avec 25 colonnes contenant des métadonnées sur les individus et le reste des colonnes correspondent à l’expression de gènes généralement impliqués dans le cancer en général.

## Méthodes utilisées

3 méthodes différentes ont été utilisées pour analyser ce jeu de données :

### K-means
Cette méthode a pour objectif de regrouper des individus ayant les caractéristiques les plus proches au sein de K groupes en fonction de la taille des tumeurs et du nombre de mutations.

### Règles d'associations
L'objectif de cette méthode est de s'intéresser aux associations pouvant mettre en relation certains attributs comme le type d’opération, type de cancer du sein , la cellularité , le statut d’ER mesuré par IHC ,l’état ménopausal et la latéralité de la tumeur primitive.

### Classification
En utilisant un arbre de décision, l'utilisation de cette méthode a pour objectif de classer le stade de la tumeur à partir des variables suivantes : le type de cancer détaillé, le niveau de cellularité, la taille de la tumeur  et les ganglions lymphatiques positifs.
